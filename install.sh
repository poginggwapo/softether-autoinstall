#!/bin/bash
# Modified by GLNEO
clear
emptyStr=""
approach=${emptyStr}
host="$(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1 -d'/')"
hubName=${emptyStr}
hubPwd=${emptyStr}
userName=${emptyStr}
userPwd=${emptyStr}

echo -n "Virtual Hub Name: "
read hubName
read -s -p "${hubName} Password: " hubPwd
echo ""
echo "Now lets create our first user."
echo -n "Username: "
read userName
read -s -p "${userName} Password: " userPwd
echo ""

while :
do
    echo "Now choose your desired Softether Setup"
    echo "[1]. LocalBridge"
    echo "[2]. SecureNat (Default)"
    echo -n "Select (1/2): "
    read approach 

    if [ $approach -eq 1  ];
        then
        echo "You choose local bridge setup"
        break;
    elif [ $approach -eq 2 ];
        then
        echo "You choose secure nat setup"
        break;
    else
        echo ""
        echo "Invalid input. Please try again."
        echo ""
    fi
done

echo ""
echo ""

sudo apt-get -y update && sudo apt-get -y upgrade
sudo apt-get install -y build-essential

wget http://www.softether-download.com/files/softether/v4.27-9668-beta-2018.05.29-tree/Linux/SoftEther_VPN_Server/64bit_-_Intel_x64_or_AMD64/softether-vpnserver-v4.27-9668-beta-2018.05.29-linux-x64-64bit.tar.gz
tar -xzf softether-vpnserver-v4.27-9668-beta-2018.05.29-linux-x64-64bit.tar.gz
rm softether-vpnserver-v4.27-9668-beta-2018.05.29-linux-x64-64bit.tar.gz
cd /root/vpnserver && make
target="/usr/local/vpnserver/"
cd ../ && mv vpnserver/ /usr/local && chmod 600 * ${target} && chmod 700 ${target}vpncmd && chmod 700 ${target}vpnserver

echo net.ipv4.ip_forward = 1 >> /etc/sysctl.conf
sysctl -w net.ipv4.ip_forward=1
sysctl --system

### SSH brute-force protection ### 
iptables -A INPUT -p tcp --dport ssh -m conntrack --ctstate NEW -m recent --set 
iptables -A INPUT -p tcp --dport ssh -m conntrack --ctstate NEW -m recent --update --seconds 60 --hitcount 10 -j DROP  
### Protection against port scanning ### 
iptables -N port-scanning 
iptables -A port-scanning -p tcp --tcp-flags SYN,ACK,FIN,RST RST -m limit --limit 1/s --limit-burst 2 -j RETURN 
iptables -A port-scanning -j DROP


if [ $approach -eq 2 ];
    then
    echo '#!/bin/sh
# description: SoftEther VPN Server
### BEGIN INIT INFO
# Provides:          vpnserver
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: softether vpnserver
# Description:       softether vpnserver daemon
### END INIT INFO
DAEMON=/usr/local/vpnserver/vpnserver
LOCK=/var/lock/subsys/vpnserver
test -x $DAEMON || exit 0
case "$1" in
start)
$DAEMON start
touch $LOCK
;;
stop)
$DAEMON stop
rm $LOCK
;;
restart)
$DAEMON stop
sleep 3
$DAEMON start
;;
*)
echo "Usage: $0 {start|stop|restart}"
exit 1
esac
exit 0' > /etc/init.d/vpnserver
fi

echo "nameserver 8.8.8.8" > "/etc/resolv.conf"
echo "nameserver 8.8.4.4" >> "/etc/resolv.conf"

chmod 755 /etc/init.d/vpnserver && /etc/init.d/vpnserver start
update-rc.d vpnserver defaults


target="/usr/local/"

sleep 2
${target}vpnserver/vpncmd localhost /SERVER /CMD ServerPasswordSet ${hubPwd}
${target}vpnserver/vpncmd localhost /SERVER /PASSWORD:${hubPwd} /CMD HubCreate ${hubName} /PASSWORD:${hubPwd}
${target}vpnserver/vpncmd localhost /SERVER /PASSWORD:${hubPwd} /HUB:${hubName} /CMD UserCreate ${userName} /GROUP:none /REALNAME:none /NOTE:none
${target}vpnserver/vpncmd localhost /SERVER /PASSWORD:${hubPwd} /HUB:${hubName} /CMD UserPasswordSet ${userName} /PASSWORD:${userPwd}
${target}vpnserver/vpncmd localhost /SERVER /PASSWORD:${hubPwd} /CMD IPsecEnable /L2TP:yes /L2TPRAW:yes /ETHERIP:no /PSK:MYVPNSEC /DEFAULTHUB:${hubName}
${target}vpnserver/vpncmd localhost /SERVER /PASSWORD:${hubPwd} /CMD HubDelete DEFAULT
${target}vpnserver/vpncmd localhost /SERVER /PASSWORD:${hubPwd} /HUB:${hubName} /CMD SecureNatEnable
${target}vpnserver/vpncmd localhost /SERVER /PASSWORD:${hubPwd} /CMD VpnOverIcmpDnsEnable /ICMP:yes /DNS:yes
${target}vpnserver/vpncmd localhost /SERVER /PASSWORD:${hubPwd} /CMD ListenerCreate 53
${target}vpnserver/vpncmd localhost /SERVER /PASSWORD:${hubPwd} /CMD ListenerCreate 137
${target}vpnserver/vpncmd localhost /SERVER /PASSWORD:${hubPwd} /CMD ListenerCreate 500
${target}vpnserver/vpncmd localhost /SERVER /PASSWORD:${hubPwd} /CMD ListenerCreate 921
${target}vpnserver/vpncmd localhost /SERVER /PASSWORD:${hubPwd} /CMD ListenerCreate 4500
${target}vpnserver/vpncmd localhost /SERVER /PASSWORD:${hubPwd} /CMD ListenerCreate 4000
${target}vpnserver/vpncmd localhost /SERVER /PASSWORD:${hubPwd} /CMD ListenerCreate 40000
clear

echo "Softether has been installed successfully"
echo "Host: ${host}"
echo "Virtual Hub: ${hubName}"
echo "Hub Password: ${hubPwd}"
echo "Port: 443, 53, 137"
echo "Username: ${userName}"
echo "Password: ${userPwd}"